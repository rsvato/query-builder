package com.appletree.utils.querybuilder;

import org.junit.Test;

import static com.appletree.utils.querybuilder.Conditions.and;
import static com.appletree.utils.querybuilder.Conditions.or;
import static com.appletree.utils.querybuilder.Expression.e;
import static com.appletree.utils.querybuilder.Field.f;
import static com.appletree.utils.querybuilder.Joins.left;
import static com.appletree.utils.querybuilder.Joins.outer;
import static com.appletree.utils.querybuilder.Query.*;
import static junit.framework.Assert.assertEquals;

public class QueryTest {

    @Test
    public void testSelectExpression() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name, ISNULL(Name) a_NameNull FROM ABC a",
                from(table("ABC").as("a").select(f("Id"), f("Name"), e("ISNULL(Name)").as("a_NameNull"))).toQuery());
    }

    @Test
    public void testGetQueryWithFields() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a", from(table("ABC").as("a").select(f("Id"), f("Name"))).toQuery());
    }

    @Test
    public void testOrderFieldsAsc() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a ORDER BY a_Id ASC", from(table("ABC").as("a").select(f("Id"), f("Name")).order("Id", true)).toQuery());
    }

    @Test
    public void testOrderFieldsDesc() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a ORDER BY a_Id DESC", from(table("ABC").as("a").select(f("Id"), f("Name")).order("Id", false)).toQuery());
    }

    @Test
    public void testOrderJoinedAsc() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name, b.Name b_Name FROM ABC a LEFT JOIN BCD b ON b.a_Id = a.Id ORDER BY a_Id DESC, b_Name ASC",
                from(table("ABC").as("a").select(f("Id"), f("Name")).order("Id", false))
                .join(left(table("BCD").as("b").select(f("Name")).order("Name", true)).on(f("b.a_Id").eq(f("a.Id"))))
                .toQuery());
    }


    @Test
    public void testLimit() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a LIMIT 10", from(table("ABC").as("a").select(f("Id"), f("Name"))).limit(10).toQuery());
    }

    @Test
    public void testLimitOffset() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a LIMIT 10 OFFSET 1", from(table("ABC").as("a").select(f("Id"), f("Name"))).limit(10).offset(1).toQuery());
    }

    @Test(expected = IllegalStateException.class)
    public void testOffsetWithoutLimit() throws Exception {
        from(table("ABC").as("a").select(f("Id"), f("Name"))).offset(1).toQuery();
    }

    @Test
    public void testWhereEquals() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE a.Id = 1",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(f("a.Id").eq("1")).toQuery());
    }

    @Test
    public void testWhereGt() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE a.Id > 1",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(f("a.Id").gt("1")).toQuery());
    }

    @Test
    public void testWhereGtField() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE a.Id > b.Id",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(f("a.Id").gt(f("b.Id"))).toQuery());
    }

    @Test
    public void testWhereNotNullField() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE a.Id IS NOT NULL",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(f("a.Id").isNotNull()).toQuery());
    }

    @Test
    public void testWhereEqualsAnd() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE a.Id = 1 AND a.Name = ?",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(and(f("a.Id").eq("1"), f("a.Name").eq("?"))).toQuery());
    }

    @Test
    public void testWhereBetweenValues() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE a.Id BETWEEN 1 AND 2 AND a.Name = ?",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(and(f("a.Id").between(1, 2), f("a.Name").eq("?"))).toQuery());
    }

    @Test
    public void testWhereValueBetweenFields() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE 1 BETWEEN a.Id AND a.Bd AND a.Name = ?",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(and(f("a.Id").betweenFields(f("a.Bd"), 1), f("a.Name").eq("?"))).toQuery());
    }

    @Test
    public void testWhereEqualsAndOr() throws Exception {
        assertEquals("SELECT a.Id a_Id, a.Name a_Name FROM ABC a WHERE a.Id = 1 AND a.Name = ? OR a.foo = 'bar'",
                from(table("ABC").as("a").select(f("Id"), f("Name"))).where(and(f("a.Id").eq("1"), f("a.Name").eq("?")))
                        .where(or(f("a.foo").eq(quote("bar")))).toQuery());
    }


    @Test
    public void testJoin() throws Exception {
        assertEquals("SELECT p.Id p_Id, p.Male p_Male, n.First n_First, n.Last n_Last FROM Person p LEFT JOIN Name n ON p.Primary_Name_Id = n.Id",
                from(table("Person").as("p").select(f("Id"), f("Male"))).join(left(
                        table("Name").as("n").select(f("First"), f("Last"))).on(f("p.Primary_Name_Id").eq(f("n.Id"))
                )).toQuery());
    }

    @Test
    public void testOuterJoin() throws Exception {
        assertEquals("SELECT p.Id p_Id, p.Male p_Male, n.First n_First, n.Last n_Last FROM Person p LEFT OUTER JOIN Name n ON p.Primary_Name_Id = n.Id",
                from(table("Person").as("p").select(f("Id"), f("Male"))).join(outer(
                        table("Name").as("n").select(f("First"), f("Last"))).on(f("p.Primary_Name_Id").eq(f("n.Id")))
                ).toQuery());
    }

    @Test
    public void testDeeperJoin() throws Exception {
        assertEquals("SELECT p.Id p_Id, p1.Id p1_Id, p2.Id p2_Id FROM Person p LEFT OUTER JOIN Person p1 ON p.Id = p1.p_Id  LEFT OUTER JOIN Person p2 ON p1.Id = p2.p_Id",
                from(table("Person").as("p").select(f("Id"))).join(outer(
                        table("Person").as("p1").select(f("Id"))).on(f("p.Id").eq(f("p1.p_Id")))
                ).join(
                                outer(table("Person").as("p2").select(f("Id"))).on(f("p1.Id").eq(f("p2.p_Id")))
                        )
                .toQuery()
        );
    }
    
    @Test
    public void testReal() throws Exception {
        Table person = table("Person").as("p").select(
                f("Id"), f("Email"), e("ISNULL(p.Male)").as("male_null"), f("Birth_Event_Id"), f("Death_Event_Id")
        );
        Table name = table("Name").as("n").select(
                f("Id"), f("First"), f("Middle"), f("Last"), f("Suffix"), f("Count")
        );
        Table image = table("Image").as("i").select(f("Id"), f("Filename"), f("Width"), f("Height"));

        Query joins = from(person).join(left(name).on(f("p.Primary_Name_Id").eq(f("n.Id"))))
                .join(outer(table("Person_In_Image").as("pi")).on(f("p.Profile_Image_in_Id").eq(f("pi.Id"))))
                        .join(outer(image).on(f("pi_Image_Id").eq(f("i.Id")))).where(and(f("p.Id").gt(1), f("n.Count").eq("?")));
        String sql = joins.toQuery();
        String expected = "SELECT p.Id p_Id, p.Email p_Email, ISNULL(p.Male) male_null, p.Birth_Event_Id p_Birth_Event_Id, p.Death_Event_Id p_Death_Event_Id, n.Id n_Id, n.First n_First, n.Middle n_Middle, n.Last n_Last, n.Suffix n_Suffix, n.Count n_Count, i.Id i_Id, i.Filename i_Filename, i.Width i_Width, i.Height i_Height FROM Person p LEFT JOIN Name n ON p.Primary_Name_Id = n.Id  LEFT OUTER JOIN Person_In_Image pi ON p.Profile_Image_in_Id = pi.Id  LEFT OUTER JOIN Image i ON pi_Image_Id = i.Id WHERE p.Id > 1 AND n.Count = ?";
        assertEquals(expected, sql);
    }

    @Test
    public void testQueryReuse() {
        Table name = table("Name").as("n").select(f("Id"), f("Name"));
        Table people = table("Person").as("p").select(f("Id"), f("Email"));
        String sql = from(name).join(left(people).on(f("n.Id").eq(f("p.Primary_Name_Id")))).toQuery();
        assertEquals("SELECT n.Id n_Id, n.Name n_Name, p.Id p_Id, p.Email p_Email FROM Name n LEFT JOIN Person p ON n.Id = p.Primary_Name_Id", sql);
        Table images = table("Image").as("i").select(f("Filename"));
        sql = from(name).join(left(images).on(f("n.Id").eq(f("i.Name_Id")))).toQuery();
        assertEquals("SELECT n.Id n_Id, n.Name n_Name, i.Filename i_Filename FROM Name n LEFT JOIN Image i ON n.Id = i.Name_Id", sql);
    }
}
