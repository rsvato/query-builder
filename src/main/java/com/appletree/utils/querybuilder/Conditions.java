package com.appletree.utils.querybuilder;

import java.util.LinkedList;
import java.util.Queue;

public class Conditions implements ToQuery{

    private Queue<Condition> conditions = new LinkedList<Condition>();

    @Override
    public String toQuery() {
        StringBuilder builder = new StringBuilder();
        Condition first = conditions.poll();
        builder.append(first.operation().toQuery());
        Condition next = conditions.poll();
        while (next != null) {
           builder.append(next.combine());
           next = conditions.poll();
        }
        return builder.toString();
    }

    public static Conditions and(Operation first, Operation... rest) {
        Conditions condition = new Conditions();
        condition.conditions.offer(new AndCondition(first));
        if (rest != null)
            for (Operation operation : rest) {
                condition.conditions.offer(new AndCondition(operation));
            }
        return condition;
    }

    public static Conditions or(Operation first, Operation... rest) {
        Conditions condition = new Conditions();
        condition.conditions.offer(new OrCondition(first));
        if (rest != null)
            for (Operation operation : rest) {
                condition.conditions.offer(new OrCondition(operation));
            }
        return condition;
    }

    public void merge(Conditions c) {
        this.conditions.addAll(c.conditions);
    }
}

interface Condition {
    Operation operation();
    String combine();
}

abstract class AbstractCondition implements Condition {
    private Operation operation;

    AbstractCondition(Operation operation) {
        this.operation = operation;
    }
    
    public Operation operation() {
        return operation;
    }
}

class AndCondition extends AbstractCondition{

    AndCondition(Operation operation) {
        super(operation);
    }

    @Override
    public String combine() {
        return " AND " + operation().toQuery();
    }
}
class OrCondition extends AbstractCondition {

    OrCondition(Operation operation) {
        super(operation);
    }

    @Override
    public String combine() {
        return " OR " + operation().toQuery();
    }
}
