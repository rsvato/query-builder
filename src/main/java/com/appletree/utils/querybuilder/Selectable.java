package com.appletree.utils.querybuilder;

public interface Selectable {
    public String toField(String tableName, String tableAlias);
}
