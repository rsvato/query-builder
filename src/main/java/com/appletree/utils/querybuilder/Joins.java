package com.appletree.utils.querybuilder;

public class Joins {
    public static Join left(Table t) {
        return new LeftJoin(t);
    }

    public static Join outer(Table t) {
        return new LeftOuterJoin(t);
    }
}
