package com.appletree.utils.querybuilder;

import com.google.common.base.Preconditions;

public class Expression implements Selectable {
    private String expression;
    private String alias;

    private Expression(){}
    
    public Expression as(String alias) {
        this.alias = alias;
        return this;
    }
    
    public static Expression e(String expression) {
        Expression result = new Expression();
        result.expression = expression;
        return result;
    }

    @Override
    public String toField(String tableName, String tableAlias) {
        Preconditions.checkNotNull(expression);
        Preconditions.checkNotNull(alias);
        return String.format("%s %s", expression, alias);
    }
}
