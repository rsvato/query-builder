package com.appletree.utils.querybuilder;

import com.google.common.base.Function;


class ToQueryStringFunction implements Function<ToQuery, String> {
    @Override
    public String apply(ToQuery input) {
        return input == null ? "" : input.toQuery();
    }
}
