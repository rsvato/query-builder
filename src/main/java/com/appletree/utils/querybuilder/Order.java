package com.appletree.utils.querybuilder;

public class Order implements ToQuery{


    private String field;
    private boolean asc;
    private Table table;

    public Order(Table table, String field, boolean asc) {
        this.table = table;
        this.field = field;
        this.asc = asc;
    }

    @Override
    public String toQuery() {
        return String.format("%s_%s %s", table.alias, field, asc ? "ASC" : "DESC");
    }
}
