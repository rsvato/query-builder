package com.appletree.utils.querybuilder;

public interface Join extends ToQuery{
    Join on(Equals<Field> eq);
    Table table();
}
class LeftJoin implements Join {
    Table table;
    Equals link;

    LeftJoin(Table t) {
        this.table = t;
    }

    public Table table() {
        return table;
    }

    public Join on(Equals eq) {
        this.link = eq;
        return this;
    }

    @Override
    public String toQuery() {
        return String.format(" LEFT JOIN %s ON %s", table.buildFrom(), link.toQuery());
    }
}

class LeftOuterJoin extends LeftJoin {
    LeftOuterJoin(Table t2) {
        super(t2);
    }

    @Override
    public String toQuery() {
        return String.format(" LEFT OUTER JOIN %s ON %s", table.buildFrom(), link.toQuery());
    }
}

