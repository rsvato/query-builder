package com.appletree.utils.querybuilder;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Joiner.on;

public class Table {
    List<Selectable> fields= new ArrayList<Selectable>();
    String name;
    String alias;
    List<Order> orders = new ArrayList<Order>();
    Optional<Conditions> conditions = Optional.absent();

    Table(String name) {
       this(name, name);
    }

    Table(String name, String alias) {
       this.name = name;
       this.alias = alias.toLowerCase();
    }

    public Table select(Selectable field, Selectable... fields) {
       this.fields.add(field);
       if (fields != null) {
           this.fields.addAll(Arrays.asList(fields));
       }
       return this;
    }

    public Table as(String alias) {
        this.alias = alias;
        return this;
    }
    
    public Table order(String field, boolean asc) {
        this.orders.add(new Order(this, field, asc));
        return this;
    }

    String buildFrom() {
        StringBuilder result = new StringBuilder(String.format("%s %s", name, alias));
        return result.toString();
    }

    String buildFields() {
        if (fields.isEmpty()) return null;
        return on(", ").join(Lists.transform(fields, new Function<Selectable, String>() {
            @Override
            public String apply(Selectable s) {
                return s.toField(name, alias);
            }
        }));
    }
    

    

}
