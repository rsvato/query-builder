package com.appletree.utils.querybuilder;

public class Field implements ToQuery, Selectable {
    String name;

    public Field(String name) {
        this.name = name;
    }

    public static Field f(String name) {
        return new Field(name);
    }

    public Equals<Field> eq(Field other) {
        return new Equals<Field>(this, other);
    }

    @Override
    public String toQuery() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public<T> Operation eq(T s) {
        return new LhsRhsOperation<Field,T>(this, s, Operations.EQ);
    }

    public<T> Operation gt(T s) {
        return new LhsRhsOperation<Field,T>(this, s, Operations.GT);
    }

    public<T> Operation lt(T s) {
        return new LhsRhsOperation<Field,T>(this, s, Operations.LT);
    }

    public<T> Operation ge(T s) {
        return new LhsRhsOperation<Field,T>(this, s, Operations.GE);
    }

    public<T> Operation le(T s) {
        return new LhsRhsOperation<Field,T>(this, s, Operations.LE);
    }

    public Operation isNull() {
        return new IsOperation<Field>(this, Operations.NULL);
    }

    public Operation isNotNull() {
        return new IsOperation<Field>(this, Operations.NOT_NULL);
    }
    
    public<T> Operation betweenFields(Field otherField, T value) {
        return new BetweenOperation<T, Field, Field>(value, this, otherField);
    }

    public<T> Operation between(T down, T up) {
        return new BetweenOperation<Field, T, T>(this, down, up);
    }

    @Override
    public String toField(String tableName, String tableAlias) {
        return String.format("%s.%s %s_%s", tableAlias, name, tableAlias, name);
    }
}
