package com.appletree.utils.querybuilder;

import com.google.common.base.*;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import static com.appletree.utils.querybuilder.Conditions.and;
import static com.google.common.base.Strings.emptyToNull;

public class Query {
    
    List<Table> tables = new ArrayList<Table>();
    private Table masterTable;
    private int limit;
    private int offset;
    private List<Join> joins = new ArrayList<Join>();
    private Optional<Conditions> conditions = Optional.absent();

    private Query(Table table) {
        masterTable = table;
        tables.add(table);
    }

    public static Table table(String name) {
        return new Table(name);
    }

    public static Query from(Table t) {
        return new Query(t);
    }
    
    public static String quote(String value) {
        return String.format("'%s'", value);
    }

    public Query limit(int count) {
       this.limit = count;
       return this;
    }

    public Query offset(int from) {
        Preconditions.checkState(limit > 0, "Offset without limit");
        this.offset = from;
        return this;
    }
    
    public String toQuery() {
        StringBuilder builder = new StringBuilder("SELECT ");
        addFields(builder);
        builder.append(" FROM ");
        builder.append(masterTable.buildFrom()).append(addJoins(joins));
        addWhere(builder);
        addOrder(builder);
        addLimit(builder);
        return builder.toString();
    }

    private String addJoins(List<Join> collected) {
        return Joiner.on(" ").join(Iterables.transform(collected, new ToQueryStringFunction()));
    }

    private void addLimit(StringBuilder builder) {
        if (limit > 0) {
            builder.append(" LIMIT ").append(limit);
            if (offset > 0)
                builder.append(" OFFSET ").append(offset);
        }
    }

    private void addOrder(StringBuilder builder) {
        Iterable<Order> orders = Iterables.concat(Lists.transform(tables, new Function<Table, List<Order>>() {
            @Override
            public List<Order> apply(Table table) {
                return table == null ? new ArrayList<Order>() : table.orders;
            }
        }));
        if (orders.iterator().hasNext()) {
            builder.append(" ORDER BY ").append(Joiner.on(", ").join(Iterables.transform(orders, new ToQueryStringFunction())));
        }
    }


    private void addWhere(StringBuilder builder) {
        Optional where = Optional.fromNullable(emptyToNull(conditions.isPresent()? conditions.get().toQuery() : ""));
        if (where.isPresent()) {
            builder.append(" WHERE ").append(where.get());
        }
    }

    private void addFields(StringBuilder builder) {
        Iterable<Table> tablesWithFields = Iterables.filter(tables, new Predicate<Table>() {
            @Override
            public boolean apply(Table input) {
                return !input.fields.isEmpty();
            }
        });
        builder.append(Joiner.on(", ").join(Iterables.transform(tablesWithFields, new Function<Table, String>() {
            @Override
            public String apply(Table input) {
                return input == null ? "" : input.buildFields();
            }
        })));
    }

    public Query join(Join on) {
        joins.add(on);
        tables.add(on.table());
        return this;
    }

    public Query where(Operation op) {
        Conditions newCond = and(op);
        if (this.conditions.isPresent()) {
            this.conditions.get().merge(newCond);
        }
        else {
            this.conditions = Optional.of(newCond);
        }
        return this;
    }

    public Query where(Conditions conditions, Conditions... rest) {
        if (this.conditions.isPresent())
            this.conditions.get().merge(conditions);
        else
            this.conditions = Optional.of(conditions);
        if (rest != null)
            for (Conditions conditions1 : rest) {
                this.conditions.get().merge(conditions1);
            }
        return this;
    }
}
