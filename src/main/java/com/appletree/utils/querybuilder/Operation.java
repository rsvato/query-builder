package com.appletree.utils.querybuilder;

public interface Operation extends ToQuery {
    String operation();
}

class Equals<T> extends LhsRhsOperation<Field, T>{
    public Equals(Field field, T other) {
        super(field, other, Operations.EQ);
    }
}

class LhsRhsOperation<L,R> implements Operation {
    private L left;
    private R right;
    private Operations operation;

    LhsRhsOperation(L left, R right, Operations op) {
        this.left = left;
        this.right = right;
        this.operation = op;
    }

    @Override
    public String toQuery() {
        return String.format("%s %s %s", left, operation.value, right);
    }

    @Override
    public String operation() {
        return operation.value;
    }
}

class IsOperation<L> implements Operation {
    private L argument;
    private Operations operation;
    
    IsOperation(L argument, Operations op) {
       this.argument = argument;
       this.operation = op;
    }

    @Override
    public String operation() {
        return operation.value;
    }

    @Override
    public String toQuery() {
        return String.format("%s IS %s", argument, operation.value);
    }
}

class BetweenOperation<V,D,U> implements Operation {
    private V value;
    private D down;
    private U up;

    BetweenOperation(V value, D down, U up) {
        this.value = value;
        this.down = down;
        this.up = up;
    }


    @Override
    public String operation() {
        return Operations.BETWEEN.value;
    }

    @Override
    public String toQuery() {
        return String.format("%s %s %s AND %s", value, Operations.BETWEEN.value, down, up);
    }
}

enum Operations {
    EQ("="), GT(">"), LT("<"), BETWEEN("BETWEEN"), GE(">="), LE("<="), NULL("NULL"), NOT_NULL("NOT NULL");

    String value;
    Operations(String s) {
       value = s;
    }
}
